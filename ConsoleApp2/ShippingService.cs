﻿using System;
using System.Collections.Generic;
using System.Text;



//Napisati klasu ShippingService koja ima metodu za računanje cijene dostave paketa.Cijena se računa na
//temelju mase/težine paketa i jedinične cijene za masu.Jedinična cijena za masu je atribut navedene klase.
namespace LV5
{
    class ShippingService
    {
        double PricePerUnitOfMass;
        public ShippingService(double PricePerUnitOfMass)
        {
            this.PricePerUnitOfMass = PricePerUnitOfMass;
        }
        public double priceperunitofmass {
            get { return this.PricePerUnitOfMass; }
            set { PricePerUnitOfMass = value; }
        }
        public double CalculatePrice (Box Packaged)
        {
            double TotalPrice = (Packaged.Weight * PricePerUnitOfMass) + Packaged.Price;
            return TotalPrice;

        }
    }
}

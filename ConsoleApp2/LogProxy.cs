﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace LV5
{
    class LogProxy : IDataset
    {
        private Dataset dataset;
        private string filePath;
        ConsoleLogger Logger;
        public LogProxy(string filePath)
        {
            this.filePath = filePath;
            Logger = ConsoleLogger.GetInstance(filePath);
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            Logger.log();
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            return dataset.GetData();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace LV5
{
    class DataConsolePrinter
    {
        public void PrintOnConsol(IDataset dataset)
        {
            ReadOnlyCollection<List<string>> datasetList = dataset.GetData();
            if (datasetList == null){ Console.WriteLine("Pristup odbijen"); }
            else { 
                foreach (List <string> data in datasetList)
                {
                    foreach(string Data in data){
                        Console.WriteLine(Data + " ");
                    }
                    Console.WriteLine("\n");
                }
            }
        }
    }
}

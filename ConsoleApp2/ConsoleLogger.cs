﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5
{
    class ConsoleLogger
    {
        private string FilePath;
        private static ConsoleLogger instance;

        private ConsoleLogger(string FilePath)
        {
            this.FilePath = FilePath;
        }
        public static ConsoleLogger GetInstance(string FilePath)
        {
            if (instance == null)
            {
                instance = new ConsoleLogger(FilePath);
            }
            return instance;
        }
        public void log() {
            Console.WriteLine("Datoteci"+ FilePath + " je pritapano u" + DateTime.Now);
        }

    }
}
